@echo off
if "%~1"=="-FIXED_CTRL_C" (
   SHIFT
) ELSE (
   CALL <NUL %0 -FIXED_CTRL_C %*
   GOTO :EOF
)
cd /d %~dp0
@call npm install
setx http_proxy "http://localhost:8000"
setx https_proxy "http://localhost:8000"
cls
node .
setx http_proxy ""
setx https_proxy ""
exit /b 0
