# DVDFabServerEmulator

A simple and efficient server emulator for DVDFab products; including DVDFab, StreamFab, MusicFab and more...

## Mirrors

_**I do not have a GitHub repository for this project. If you see this project on GitHub, it is an unofficial mirror. The only official source is [The CDM-Project](https://cdm-project.com).**_

## Installation

Download the [latest distribution (`DVDFabServerEmulator.zip`)](https://cdm-project.com/Deja/DVDFabServerEmulator/releases/latest) and unpack it to a directory you can easily access.

Afterwards, run the runtime script (`start.bat`) and enjoy! You can now install and use any DVDFab software locally without having to connect to any external authentication servers.

Please note that you will need to keep the emulator/proxy running whilst using DVDFab software.

_**It is a requirement for you to press (`CTRL-C`) to disconnect from the local proxy after you're done using it, as it can interfere with other programs.**_

_If you closed the emulator without pressing (`CTRL-C`), you can run the clean-up script (`stop.bat`)._

## Notes

This project is not affiliated with StreamFab or any other subsidiary of DVDFab, and does not contain any copyrighted work.

This software is only intended to allow legitimately licensed users of the DVDFab software suite to use their rightfully purchased software without having to connect to any external authentication servers, it is not intended to promote or encourage piracy of any kind, and should only be used by legitimate owners of DVDFab software.
